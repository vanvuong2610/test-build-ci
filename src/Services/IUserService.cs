﻿namespace DemoGitLabCI.Services
{
    public interface IUserService
    {
        List<string> Users { get; }
    }
}
