﻿namespace DemoGitLabCI.Services
{
    public class UserService : IUserService
    {
        private List<string> _userCollection = new List<string>()
        {
            "Vương Nguyễn",
            "Giang Đỗ",
            "Trung Lê",
            "Việt Hồ",
            "Việt Phùng",
            "Nga Trần",
            "Huy Nguyễn",
            "Trang Huỳnh",
            "Xuân Diệu",
            "Hiếu Trần",
            "Trung Lê"
        };

        public List<string> Users => _userCollection;
    }
}
